package eltemps;

import eltemps.domain.City;
import eltemps.domain.Weather;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuBar;

public class InicialController {

    private WeatherService weatherService = new WeatherService();

    @FXML
    private Label lbInfo;
    @FXML
    ListView cityList;


    public void initialize(){
        cityList.getItems().add("Barcelona");
        cityList.getItems().add("Ottawa");
        cityList.getItems().add("Sidney");
        cityList.getItems().add("Oslo");

        cityList.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    selectCity(newValue.toString());
                }
        );

    }

    public void testJSON(ActionEvent actionEvent) {

        cityList.getItems().add("Barcelona");
        cityList.getItems().add("Ottawa");

    }

    public void selectCity(String city){

        Weather current = weatherService.getCurrentWeather(city);

        if (current != null) {
            lbInfo.setText("Temp: " + current.getTemp() +
                    "\nFeels like: " + current.getFeelsLike() +
                    "\nMin: " + current.getMin() +
                    "\nMax: " + current.getMax() +
                    "\nPressure: " + current.getPressure() +
                    "\nHumidity: " + current.getHumidity());
        }


    }







}
